#pragma once

#include <glew.h>
#include <iostream>
#include <vector>
#include "..//utils/fileutils.h"
#include "..//maths/maths.h"

namespace StarEngine
{
	namespace Graphics
	{
		class Shader
		{
		private:
			GLuint m_ShaderID;
			const char* m_VertPath;
			const char* m_FragPath;
		public:
			Shader(const char* vertPath, const char* fragPath);
			~Shader();

			void SetUniform1f(const GLchar* name, float value);
			void SetUniform1i(const GLchar* name, int value);
			void SetUniform2f(const GLchar* name, const Maths::Vec2& vector);
			void SetUniform3f(const GLchar* name, const Maths::Vec3& vector);
			void SetUniform4f(const GLchar* name, const Maths::Vec4& vector);
			void SetUniformMat4(const GLchar* name, const Maths::Mat4& matrix);

			void Enable() const;
			void Disable() const;

		private:
			GLuint Load();
			GLint GetUniformLocation(const GLchar* name);
		};
	}
}