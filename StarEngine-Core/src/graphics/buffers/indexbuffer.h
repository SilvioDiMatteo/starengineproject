#pragma once

#include <glew.h>

namespace StarEngine
{
	namespace Graphics
	{
		class IndexBuffer
		{
		private:
			GLuint m_BufferID;
			GLuint m_Count;
		public:
			IndexBuffer(GLushort* data, GLsizei count);

			void Bind() const;
			void Unbind() const;
			inline GLuint GetCount() const { return m_Count; }
		};
	}
}