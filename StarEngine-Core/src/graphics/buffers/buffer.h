#pragma once

#include <glew.h>

namespace StarEngine
{
	namespace Graphics
	{
		class Buffer
		{
		private:
			GLuint m_BufferID;
			GLuint m_ComponentCount;
		public:
			Buffer(GLfloat* data, GLsizei count, GLuint componentCount);

			void Bind() const;
			void Unbind() const;
			inline GLuint GetComponentCount() const { return m_ComponentCount; }
		};
	}
}