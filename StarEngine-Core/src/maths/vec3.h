#pragma once

#include <iostream>
#include "vec2.h"

namespace StarEngine
{
	namespace Maths
	{
		struct Vec3
		{
			float x, y, z;

			Vec3();
			Vec3(const float& x, const float& y, const float& z);

			Vec3& Add(const Vec3& other);
			Vec3& Add(const Vec2& other);
			Vec3& Subtract(const Vec3& other);
			Vec3& Subtract(const Vec2& other);
			Vec3& Multiply(const Vec3& other);
			Vec3& Multiply(const Vec2& other);
			Vec3& Divide(const Vec3& other);
			Vec3& Divide(const Vec2& other);

			friend Vec3 operator+(Vec3 left, const Vec3& right);
			friend Vec3 operator+(Vec3 left, const Vec2& right);
			friend Vec3 operator-(Vec3 left, const Vec3& right);
			friend Vec3 operator-(Vec3 left, const Vec2& right);
			friend Vec3 operator*(Vec3 left, const Vec3& right);
			friend Vec3 operator*(Vec3 left, const Vec2& right);
			friend Vec3 operator/(Vec3 left, const Vec3& right);
			friend Vec3 operator/(Vec3 left, const Vec2& right);

			Vec3& operator+=(const Vec3& other);
			Vec3& operator+=(const Vec2& other);
			Vec3& operator-=(const Vec3& other);
			Vec3& operator-=(const Vec2& other);
			Vec3& operator*=(const Vec3& other);
			Vec3& operator*=(const Vec2& other);
			Vec3& operator/=(const Vec3& other);
			Vec3& operator/=(const Vec2& other);

			bool operator==(const Vec3& other);
			bool operator!=(const Vec3& other);

			friend std::ostream& operator<<(std::ostream& stream, const Vec3& vector);
		};
	}
}