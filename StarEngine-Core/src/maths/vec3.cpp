#include "vec3.h"

namespace StarEngine
{
	namespace Maths
	{
		Vec3::Vec3()
		{
			x = 0;
			y = 0;
			z = 0;
		}

		Vec3::Vec3(const float& x, const float& y, const float& z)
		{
			this->x = x;
			this->y = y;
			this->z = z;
		}

		Vec3& Vec3::Add(const Vec3& other)
		{
			x += other.x;
			y += other.y;
			z += other.z;
			return *this;
		}

		Vec3& Vec3::Add(const Vec2& other)
		{
			x += other.x;
			y += other.y;
			return *this;
		}

		Vec3& Vec3::Subtract(const Vec3& other)
		{
			x -= other.x;
			y -= other.y;
			z -= other.z;
			return *this;
		}

		Vec3& Vec3::Subtract(const Vec2& other)
		{
			x -= other.x;
			y -= other.y;
			return *this;
		}

		Vec3& Vec3::Multiply(const Vec3& other)
		{
			x *= other.x;
			y *= other.y;
			z *= other.z;
			return *this;
		}

		Vec3& Vec3::Multiply(const Vec2& other)
		{
			x *= other.x;
			y *= other.y;
			return *this;
		}

		Vec3& Vec3::Divide(const Vec3& other)
		{
			x /= other.x;
			y /= other.y;
			z /= other.z;
			return *this;
		}

		Vec3& Vec3::Divide(const Vec2& other)
		{
			x /= other.x;
			y /= other.y;
			return *this;
		}

#pragma region Operators
			   
		Vec3 operator+(Vec3 left, const Vec3& right)
		{
			return left.Add(right);
		}

		Vec3 operator+(Vec3 left, const Vec2& right)
		{
			return left.Add(right);
		}

		Vec3 operator-(Vec3 left, const Vec3& right)
		{
			return left.Subtract(right);
		}

		Vec3 operator-(Vec3 left, const Vec2& right)
		{
			return left.Subtract(right);
		}
		
		Vec3 operator*(Vec3 left, const Vec3& right)
		{
			return left.Multiply(right);
		}

		Vec3 operator*(Vec3 left, const Vec2& right)
		{
			return left.Multiply(right);
		}
		
		Vec3 operator/(Vec3 left, const Vec3& right)
		{
			return left.Divide(right);
		}

		Vec3 operator/(Vec3 left, const Vec2& right)
		{
			return left.Divide(right);
		}

		Vec3& Vec3::operator+=(const Vec3& other)
		{
			return Add(other);
		}

		Vec3& Vec3::operator+=(const Vec2& other)
		{
			return Add(other);
		}

		Vec3& Vec3::operator-=(const Vec3& other)
		{
			return Subtract(other);
		}

		Vec3& Vec3::operator-=(const Vec2& other)
		{
			return Add(other);
		}

		Vec3& Vec3::operator*=(const Vec3& other)
		{
			return Multiply(other);
		}

		Vec3& Vec3::operator*=(const Vec2& other)
		{
			return Add(other);
		}

		Vec3& Vec3::operator/=(const Vec3& other)
		{
			return Divide(other);
		}

		Vec3& Vec3::operator/=(const Vec2& other)
		{
			return Add(other);
		}

		bool Vec3::operator==(const Vec3& other)
		{
			return x == other.x && y == other.y && z == other.z;
		}

		bool Vec3::operator!=(const Vec3& other)
		{
			return !(*this == other);
		}

		std::ostream& operator<<(std::ostream& stream, const Vec3& vector)
		{
			stream << "Vec3: (" << vector.x << ", " << vector.y << ", " << vector.z << ")";
			return stream;
		}

#pragma endregion
	
	}
}