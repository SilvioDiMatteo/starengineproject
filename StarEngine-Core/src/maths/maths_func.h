#pragma once

#define _USE_MATH_DEFINES
#include <math.h>

namespace StarEngine
{
	namespace Maths
	{
		inline float toRadians(float degress)
		{
			return degress * (M_PI / 180.0f);
		}
	}
}