#pragma once

#include "maths_func.h"
#include "vec3.h"
#include "vec4.h"


namespace StarEngine
{
	namespace Maths
	{
#define MAT_SIZE 4

		struct Mat4
		{
			union 
			{
				float elements[MAT_SIZE * MAT_SIZE];
				Vec4 columns[MAT_SIZE];
			};

			Mat4();
			Mat4(float diagonal);

			static Mat4 Identity();

			Mat4& Multiply(const Mat4& other);
			friend Mat4 operator*(Mat4 left, const Mat4& right);
			Mat4& operator*=(const Mat4& other);

			static Mat4 Orthographic(float left, float right, float bottom, float top, float near, float far);
			static Mat4 Perspective(float fov, float aspectRatio, float near, float far);

			static Mat4 Translate(const Vec3& translation);
			static Mat4 Rotate(float angle, const Vec3& axis);
			static Mat4 Scale(const Vec3& scale);
		};
	}
}