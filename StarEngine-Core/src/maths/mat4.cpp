#include "mat4.h"

namespace StarEngine
{
	namespace Maths
	{
		Mat4::Mat4()
		{
			for (int i = 0; i < MAT_SIZE * MAT_SIZE; i++)
			{
				elements[i] = 0.0f;
			}
		}

		Mat4::Mat4(float diagonal)
		{
			for (int i = 0; i < MAT_SIZE * MAT_SIZE; i++)
			{
				if (i % (MAT_SIZE + 1) == 0)
				{
					elements[i] = diagonal;
					continue;
				}
				elements[i] = 0.0f;
			}
		}

		Mat4 Mat4::Identity()
		{
			return Mat4(1.0f);
		}

		Mat4& Mat4::Multiply(const Mat4& other)
		{
			for (int x = 0; x < MAT_SIZE; x++)
			{
				for (int y = 0; y < MAT_SIZE; y++)
				{
					float sum = 0.0f;
					for (int e = 0; e < MAT_SIZE; e++)
					{
						sum += elements[x + y * MAT_SIZE] * other.elements[e + y * MAT_SIZE];
					}
					elements[x + y * MAT_SIZE] = sum;
				}
			}
			return *this;
		}

		Mat4 operator*(Mat4 left, const Mat4& right)
		{
			return left.Multiply(right);
		}

		Mat4& Mat4::operator*=(const Mat4& other)
		{
			return Multiply(other);
		}

		Mat4 Mat4::Orthographic(float left, float right, float bottom, float top, float near, float far)
		{
			Mat4 result(1.0f);

			result.elements[0 + 0 * MAT_SIZE] = 2.0f / (right - left);
			result.elements[1 + 1 * MAT_SIZE] = 2.0f / (top - bottom);
			result.elements[2 + 2 * MAT_SIZE] = 2.0f / (near - far);

			result.elements[0 + 3 * MAT_SIZE] = (left + right) / (left - right);
			result.elements[1 + 3 * MAT_SIZE] = (bottom + top) / (bottom - top);
			result.elements[2 + 3 * MAT_SIZE] = (far + near) / (far - near);

			return result;
		}

		Mat4 Mat4::Perspective(float fov, float aspectRatio, float near, float far)
		{
			Mat4 result(1.0f);

			float q = 1.0f / tan(toRadians(0.5f * fov));
			float a = q / aspectRatio;
			float b = (near + far) / (near - far);
			float c = (2.0f * near * far) / (near - far);

			result.elements[0 + 0 * MAT_SIZE] = a;
			result.elements[1 + 1 * MAT_SIZE] = q;
			result.elements[2 + 2 * MAT_SIZE] = b;
			result.elements[3 + 2 * MAT_SIZE] = -1.0f;
			result.elements[2 + 3 * MAT_SIZE] = c;

			return result;
		}

		Mat4 Mat4::Translate(const Vec3& translation)
		{
			Mat4 result(1.0f);

			result.elements[0 + 3 * MAT_SIZE] = translation.x;
			result.elements[1 + 3 * MAT_SIZE] = translation.y;
			result.elements[2 + 3 * MAT_SIZE] = translation.z;

			return result;
		}

		Mat4 Mat4::Rotate(float angle, const Vec3& axis)
		{
			Mat4 result(1.0f);

			float r = toRadians(angle);
			float c = cos(r);
			float s = sin(r);
			float omc = 1.0f - c;

			float x = axis.x;
			float y = axis.y;
			float z = axis.z;

			result.elements[0 + 0 * MAT_SIZE] = x * omc + c;
			result.elements[1 + 0 * MAT_SIZE] = y * x * omc + z * s;
			result.elements[2 + 0 * MAT_SIZE] = x * z * omc - y * s;

			result.elements[0 + 1 * MAT_SIZE] = x * y * omc - z * s;
			result.elements[1 + 1 * MAT_SIZE] = y * omc + c;
			result.elements[2 + 1 * MAT_SIZE] = y * z * omc + x * s;

			result.elements[0 + 2 * MAT_SIZE] = x * z * omc - y * s;
			result.elements[1 + 2 * MAT_SIZE] = y * z * omc - x * s;
			result.elements[2 + 2 * MAT_SIZE] = z * omc + c;

			return result;
		}

		Mat4 Mat4::Scale(const Vec3& scale)
		{
			Mat4 result(1.0f);

			result.elements[0 + 0 * MAT_SIZE] = scale.x;
			result.elements[1 + 1 * MAT_SIZE] = scale.y;
			result.elements[2 + 2 * MAT_SIZE] = scale.z;

			return result;
		}
	}
}