#pragma once

#include <iostream>
#include "vec2.h"
#include "vec3.h"

namespace StarEngine
{
	namespace Maths
	{
		struct Vec4
		{
			float x, y, z, w;

			Vec4();
			Vec4(const float& x, const float& y, const float& z, const float& w);

			Vec4& Add(const Vec4& other);
			Vec4& Add(const Vec2& other);
			Vec4& Add(const Vec3& other);
			Vec4& Subtract(const Vec4& other);
			Vec4& Subtract(const Vec2& other);
			Vec4& Subtract(const Vec3& other);
			Vec4& Multiply(const Vec4& other);
			Vec4& Multiply(const Vec2& other);
			Vec4& Multiply(const Vec3& other);
			Vec4& Divide(const Vec4& other);
			Vec4& Divide(const Vec2& other);
			Vec4& Divide(const Vec3& other);

			friend Vec4 operator+(Vec4 left, const Vec4& right);
			friend Vec4 operator+(Vec4 left, const Vec2& right);
			friend Vec4 operator+(Vec4 left, const Vec3& right);
			friend Vec4 operator-(Vec4 left, const Vec4& right);
			friend Vec4 operator-(Vec4 left, const Vec2& right);
			friend Vec4 operator-(Vec4 left, const Vec3& right);
			friend Vec4 operator*(Vec4 left, const Vec4& right);
			friend Vec4 operator*(Vec4 left, const Vec2& right);
			friend Vec4 operator*(Vec4 left, const Vec3& right);
			friend Vec4 operator/(Vec4 left, const Vec4& right);
			friend Vec4 operator/(Vec4 left, const Vec2& right);
			friend Vec4 operator/(Vec4 left, const Vec3& right);

			Vec4& operator+=(const Vec4& other);
			Vec4& operator+=(const Vec2& other);
			Vec4& operator+=(const Vec3& other);
			Vec4& operator-=(const Vec4& other);
			Vec4& operator-=(const Vec2& other);
			Vec4& operator-=(const Vec3& other);
			Vec4& operator*=(const Vec4& other);
			Vec4& operator*=(const Vec2& other);
			Vec4& operator*=(const Vec3& other);
			Vec4& operator/=(const Vec4& other);
			Vec4& operator/=(const Vec2& other);
			Vec4& operator/=(const Vec3& other);

			bool operator==(const Vec4& other);
			bool operator!=(const Vec4& other);

			friend std::ostream& operator<<(std::ostream& stream, const Vec4& vector);
		};
	}
}